#/bin/python3
import sys

source = open('Dockerfile').readlines()
dest = open('Dockerfile', 'w')
for line in source:
    if line.startswith('FROM greyrook/cde'):
        line, version = line.split(':')
        dest.write(line + ':')
        dest.write(sys.argv[1] + '\n')
    else:
        dest.write(line)

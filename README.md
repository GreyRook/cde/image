

# Work Shell

IMAGE=
docker run --rm -ti --net=host --user $UID -v /home:/home -e HOME=$HOME --workdir=`pwd` $IMAGE bash


# Images

## Python

## Javascript

## deploy-k8s

Image used to execute infrastruture related dasks

Stack:
 * terraform 0.11.11
 * kubectl 1.13.3
 * ansible 2.5
 * helm 2.12.3
 * openstack cli tools, incl. shade


Docker Hub: https://hub.docker.com/r/greyrook/cde-deploy-k8s/
